//package eingabeAusgabe;

/** Variablen.java
 Ergaenzen Sie nach jedem Kommentar jeweils den Quellcode.
 @author
 @version
 */
public class Variablen {
    public static void main(String [] args){
    /* 1. Ein Zaehler soll die Programmdurchlaeufe zaehlen.
          Vereinbaren Sie eine geeignete Variable */
        int zaehler;

    /* 2. Weisen Sie dem Zaehler den Wert 25 zu
          und geben Sie ihn auf dem Bildschirm aus.*/
        zaehler = 25;
        System.out.println(zaehler);

    /* 3. Durch die Eingabe eines Buchstabens soll der Menuepunkt
          eines Programms ausgewaehlt werden.
          Vereinbaren Sie eine geeignete Variable */
        char auswahlMenuepunkt;

    /* 4. Weisen Sie dem Buchstaben den Wert 'C' zu
          und geben Sie ihn auf dem Bildschirm aus.*/
        auswahlMenuepunkt = 'C';
        System.out.println(auswahlMenuepunkt);

    /* 5. Fuer eine genaue astronomische Berechnung sind grosse Zahlenwerte
          notwendig.
          Vereinbaren Sie eine geeignete Variable */
        double astronomischeZahl;

    /* 6. Weisen Sie der Zahl den Wert der Lichtgeschwindigkeit zu
          und geben Sie sie auf dem Bildschirm aus.*/
        astronomischeZahl = 299792458;
        System.out.println(astronomischeZahl);

    /* 7. Sieben Personen gruenden einen Verein. Fuer eine Vereinsverwaltung
          soll die Anzahl der Mitglieder erfasst werden.
          Vereinbaren Sie eine geeignete Variable und initialisieren sie
          diese sinnvoll.*/
        int anzahlVereinsmitglieder = 7;

        /* 8. Geben Sie die Anzahl der Mitglieder auf dem Bildschirm aus.*/
        System.out.println(anzahlVereinsmitglieder);

    /* 9. Fuer eine Berechnung wird die elektrische Elementarladung benoetigt.
          Vereinbaren Sie eine geeignete Variable und geben Sie sie auf
          dem Bildschirm aus.*/
        final double ELEKTRISCHE_ELEMENTARLADUNG = 1.602176634*Math.pow(10, -19);
        System.out.println(ELEKTRISCHE_ELEMENTARLADUNG);

    /*10. Ein Buchhaltungsprogramm soll festhalten, ob eine Zahlung erfolgt ist.
          Vereinbaren Sie eine geeignete Variable. */
        boolean istZahlungErfolgt;

    /*11. Die Zahlung ist erfolgt.
          Weisen Sie der Variable den entsprechenden Wert zu
          und geben Sie die Variable auf dem Bildschirm aus.*/
        istZahlungErfolgt = true;
        System.out.println(istZahlungErfolgt);

    }//main
}// Variablen