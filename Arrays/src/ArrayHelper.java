public class ArrayHelper {

    public static void main(String[] args) {

        int[] zahlen = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
        System.out.println(convertArrayToString(zahlen));
    }

    public static String convertArrayToString(int[] zahlen) {
        StringBuilder result = new StringBuilder();
        for (int i = 0; i < zahlen.length; i++) {
            result.append(zahlen[i]);
            if(i != zahlen.length - 1) {
                result.append(',');
                result.append(' ');
            }
        }
        return result.toString();
    }

}
