public class Aufgabe6 {

    public static void main(String[] args) {

        int[][] matrix = {{0, 1, 2}, {1, 0, 1}, {2, 1, 0}};
        int[][] matrix2 = {{0, 1, 1, 1}, {1, 0, 1, 1}, {1, 1, 0, 1}, {1, 1, 1, 0}};
        System.out.println(istTransponiertZuSichSelber(matrix));
        System.out.println(istTransponiertZuSichSelber(matrix2));
    }

    //Gibt true zurück wenn die Matrix mit sich selbst transponierbar ist
    public static boolean istTransponiertZuSichSelber(int[][] matrix) {
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < i; j++) {
                if (matrix[i][j] != matrix[j][i]) {
                    return false;
                }
            }
        }
        return true;
    }

}
