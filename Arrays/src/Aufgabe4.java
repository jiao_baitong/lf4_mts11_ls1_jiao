public class Aufgabe4 {

    public static void main(String[] args) {
        double [][] temperaturTabelle = temperaturTabelleAusgeben(10);
        for (int i = 0; i < temperaturTabelle.length; i++) {
            for (int j = 0; j < temperaturTabelle[i].length; j++) {
                System.out.printf("%6.2f", temperaturTabelle[i][j]);
                System.out.print(" ");
            }
            System.out.println();
        }
    }

    public static double[][] temperaturTabelleAusgeben(int anzahl) {
        double[][] temperaturTabelle = new double[anzahl][2];
        double ersteTemperatur = 0;
        for (int i = 0; i < anzahl; i++) {
            if (i != 0)
                temperaturTabelle[i][0] = ersteTemperatur;
            double celsiusWert = (5.0/9.0) * (temperaturTabelle[i][0] - 32.0);
            temperaturTabelle[i][1] = celsiusWert;
            ersteTemperatur += 10;
        }
        return temperaturTabelle;
    }

}
