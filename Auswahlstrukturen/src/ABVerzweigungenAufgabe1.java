import java.util.Scanner;

public class ABVerzweigungenAufgabe1 {

    public static final Scanner input = new Scanner(System.in);

    public static void main(String[] args) {
        programmAuswahl();
    }

    public static void programmAuswahl(){
        System.out.println("Welches Programm möchten Sie ausführen? (geben Sie die Zahl des Programms an)");
        System.out.println("1: Aufgabe 1.2.1, Temperaturvergleich");
        System.out.println("2: Aufgabe 1.2.2, Sind Zahlen gleich");
        System.out.println("3: Aufgabe 1.2.3, Ist Zahl größer als andere Zahl");
        System.out.println("4: Aufgabe 1.2.4, Ist Zahl größer gleich andere Zahl");
        System.out.println("5: Aufgabe 1.3.1, Ist Zahl größer als zweite Zahl und dritte Zahl");
        System.out.println("6: Aufgabe 1.3.2, Ist Zahl größer als eine Zahl oder ist Zahl größer als andere Zahl");
        System.out.println("7: Aufgabe 1.3.3, Größte von drei Zahlen");
        int eingabe = input.nextInt();
        if (eingabe == 1)
            aufgabe1punkt2punkt1();
        else if (eingabe == 2)
            aufgabe1punkt2punkt2();
        else if (eingabe == 3)
            aufgabe1punkt2punkt3();
        else if (eingabe == 4)
            aufgabe1punkt2punkt4();
        else if (eingabe == 5)
            aufgabe1punkt3punkt1();
        else if (eingabe == 6)
            aufgabe1punkt3punkt2();
        else if (eingabe == 7)
            aufgabe1punkt3punkt3();
    }


    public static void aufgabe1punkt2punkt1() {
        System.out.println("Das Program stellt die Heizung höher, wenn die Raum temperatur unter eine bestimmte Temperatur fällt.");
        System.out.println("Geben Sie die jetzige Raumtemperatur in Grad Celsius an (Zahlenwert): ");
        double raumtemperatur = input.nextDouble();
        System.out.println("Geben Sie die Ziel-Raumtemperatur in Grad Celsius an (Zahlenwert): ");
        double zieltemperatur = input.nextDouble();
        if (raumtemperatur < zieltemperatur)
            System.out.println("Die Heizung wird hochgestellt.");
        else System.out.println("Es ist warm genug.");
    }

    public static void aufgabe1punkt2punkt2() {
        System.out.println("Das Program ermittelt, ob zwei Zahlen gleich sind.");
        System.out.println("Geben Sie die erste Zahl an: ");
        double ersteZahl = input.nextDouble();
        System.out.println("Geben Sie die zweite Zahl an: ");
        double zweiteZahl = input.nextDouble();
        if(ersteZahl == zweiteZahl)
            System.out.println("Beide Zahlen sind gleich.");
    }

    public static void aufgabe1punkt2punkt3() {
        System.out.println("Das Program ermittelt, ob die zweite Zahl größer als die erste Zahl ist.");
        System.out.println("Geben Sie die erste Zahl an: ");
        double ersteZahl = input.nextDouble();
        System.out.println("Geben Sie die zweite Zahl an: ");
        double zweiteZahl = input.nextDouble();
        if(ersteZahl < zweiteZahl)
            System.out.println("Die zweite Zahl ist größer als die erste Zahl.");
    }

    public static void aufgabe1punkt2punkt4() {
        System.out.println("Das Program ermittelt, ob die erste Zahl größer gleich die zweite Zahl ist.");
        System.out.println("Geben Sie die erste Zahl an: ");
        double ersteZahl = input.nextDouble();
        System.out.println("Geben Sie die zweite Zahl an: ");
        double zweiteZahl = input.nextDouble();
        if(ersteZahl >= zweiteZahl)
            System.out.println("Die erste Zahl ist größer gleich die zweite Zahl.");
        else System.out.println("Die erste Zahl ist kleiner als die zweite Zahl.");
    }

    public static void aufgabe1punkt3punkt1() {
        System.out.println("Das Program ermittelt, ob die erste Zahl größer als die zweite Zahl und die dritte Zahl ist.");
        System.out.println("Geben Sie die erste Zahl an: ");
        double ersteZahl = input.nextDouble();
        System.out.println("Geben Sie die zweite Zahl an: ");
        double zweiteZahl = input.nextDouble();
        System.out.println("Geben Sie die dritte Zahl an: ");
        double dritteZahl = input.nextDouble();
        if(ersteZahl > zweiteZahl && ersteZahl > dritteZahl)
            System.out.println("Die erste Zahl ist größer als die zweite Zahl und die dritte Zahl.");
    }

    public static void aufgabe1punkt3punkt2() {
        System.out.println("Das Program ermittelt, ob die dritte Zahl größer als die zweite Zahl oder ob die dritte Zahl größer als die erste Zahl ist.");
        System.out.println("Geben Sie die erste Zahl an: ");
        double ersteZahl = input.nextDouble();
        System.out.println("Geben Sie die zweite Zahl an: ");
        double zweiteZahl = input.nextDouble();
        System.out.println("Geben Sie die dritte Zahl an: ");
        double dritteZahl = input.nextDouble();
        if(dritteZahl > zweiteZahl || ersteZahl < dritteZahl)
            System.out.println("Die dritte Zahl ist größer als die zweite Zahl oder die dritte Zahl ist größer als die erste Zahl.");
    }

    public static void aufgabe1punkt3punkt3() {
        System.out.println("Das Program ermittelt die größte von drei Zahlen.");
        System.out.println("Geben Sie die erste Zahl an: ");
        double ersteZahl = input.nextDouble();
        System.out.println("Geben Sie die zweite Zahl an: ");
        double zweiteZahl = input.nextDouble();
        System.out.println("Geben Sie die dritte Zahl an: ");
        double dritteZahl = input.nextDouble();
        if (ersteZahl > zweiteZahl && ersteZahl > dritteZahl)
            System.out.println(ersteZahl + " ist am größten.");
        else if (zweiteZahl > dritteZahl)
            System.out.println(zweiteZahl + " ist am größten.");
        else System.out.println(dritteZahl + " ist am größten.");
    }

}
