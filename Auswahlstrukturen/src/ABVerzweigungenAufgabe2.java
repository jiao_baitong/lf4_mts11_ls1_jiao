import java.util.Scanner;

public class ABVerzweigungenAufgabe2 {

    public static final Scanner input = new Scanner(System.in);

    public static void main(String[] args) {
        steuersatzWaehlenUndBruttobetragberechnen();
    }

    public static void steuersatzWaehlenUndBruttobetragberechnen() {
        System.out.println("Das Programm bestimmt zu einem Netteobetrag den Bruttobetrag.");
        System.out.println("Geben Sie Ihren Nettobetrag ein: ");
        double nettoBetrag = input.nextDouble();
        System.out.println("Soll der ermäßigte Steuersatz angewendet werden? (j/n)");
        if(input.next().equals("j"))
            System.out.println(nettoBetrag * 1.07 + " ist Ihr Bruttobetrag.");
        else System.out.println(nettoBetrag * 1.19 + " ist Ihr Bruttobetrag.");
    }
}
