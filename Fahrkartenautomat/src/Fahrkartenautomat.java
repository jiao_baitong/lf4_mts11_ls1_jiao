import java.util.Scanner;

class Fahrkartenautomat
{
    private static final Scanner tastatur = new Scanner(System.in);
    private static String[] FAHRKARTENBEZEICHNUNGSTABELLE = {"Einzelfahrschein Berlin AB", "Einzelfahrschein Berlin BC",
            "Einzelfahrschein Berlin ABC", "Kurzstrecke", "Tageskarte Berlin AB",
            "Tageskarte Berlin BC", "Tageskarte Berlin ABC", "Kleingruppen-Tageskarte Berlin AB",
        "Kleingruppen-Tageskarte Berlin BC", "Kleingruppen-Tageskarte Berlin ABC"};
    private static double[] FAHRKARTENPREISTABELLE = {2.90, 3.30, 3.60, 1.90, 8.60, 9.00, 9.60, 23.50, 24.30, 24.90};

    public static void main(String[] args)
    {
        while(true) {
            double zuZahlen = fahrkartenbestellungErfassen();
            double rueckgabebetrag = fahrkartenBezahlen(zuZahlen);
            fahrkartenAusgeben(rueckgabebetrag);
        }
    }

    public static double fahrkartenbestellungErfassen() {
        System.out.println("Willkommen, wählen Sie Ihr Ticket:");
        System.out.print("Auswahlnummer");
        System.out.printf("%26s", "Bezeichnung" + "\t");
        System.out.println("Preis in Euro");

        for (int i = 0; i < FAHRKARTENBEZEICHNUNGSTABELLE.length; i++) {
            System.out.print(i + 1 + "\t");
            System.out.printf("%35s", FAHRKARTENBEZEICHNUNGSTABELLE[i] + "\t");
            System.out.println(FAHRKARTENPREISTABELLE[i]);
        }
        int gewaehltesTicket;
        do {
            System.out.print("Ihre Wahl: ");
            gewaehltesTicket = tastatur.nextInt();
            if (gewaehltesTicket > 10 || gewaehltesTicket < 1)
                System.out.println(">>falsche Eingabe<<");
        } while (gewaehltesTicket > 10 || gewaehltesTicket < 1);
        gewaehltesTicket -= 1;
        double zuZahlenderBetrag;
        zuZahlenderBetrag = FAHRKARTENPREISTABELLE[gewaehltesTicket];
        System.out.print("Anzahl der Tickets: ");
        return zuZahlenderBetrag * tastatur.nextShort();
    }
    public static double fahrkartenBezahlen(double zuZahlen) {
        double eingezahlterGesamtbetrag = 0.0;
        while(eingezahlterGesamtbetrag < zuZahlen)
        {
            System.out.print("Noch zu zahlen: ");
            System.out.printf("%.2f" , (zuZahlen - eingezahlterGesamtbetrag));
            System.out.println(" Euro");
            System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
            double eingeworfeneMünze = tastatur.nextDouble();
            eingezahlterGesamtbetrag += eingeworfeneMünze;
        }
        return eingezahlterGesamtbetrag - zuZahlen;
    }
    public static void fahrkartenAusgeben(double rueckgabebetrag) {
        System.out.println("\nFahrschein wird ausgegeben");
        for (int i = 0; i < 8; i++)
        {
            System.out.print("=");
            try {
                Thread.sleep(250);
            } catch (InterruptedException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
        System.out.println("\n\n");
        rueckgeldAusgeben(rueckgabebetrag);
        System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
                "vor Fahrtantritt entwerten zu lassen!\n"+
                "Wir wünschen Ihnen eine gute Fahrt.\n");
    }
    public static void rueckgeldAusgeben(double rueckgabebetrag) {
        if(rueckgabebetrag > 0.0)
        {
            System.out.print("Der Rückgabebetrag in Höhe von ");
            System.out.printf( "%.2f" , rueckgabebetrag);
            System.out.println(" Euro");
            System.out.println("wird in folgenden Münzen ausgezahlt:");

            while(rueckgabebetrag >= 2.0) // 2 EURO-Münzen
            {
                System.out.println("2 EURO");
                rueckgabebetrag -= 2.0;
            }
            while(rueckgabebetrag >= 1.0) // 1 EURO-Münzen
            {
                System.out.println("1 EURO");
                rueckgabebetrag -= 1.0;
            }
            while(rueckgabebetrag >= 0.5) // 50 CENT-Münzen
            {
                System.out.println("50 CENT");
                rueckgabebetrag -= 0.5;
            }
            while(rueckgabebetrag >= 0.2) // 20 CENT-Münzen
            {
                System.out.println("20 CENT");
                rueckgabebetrag -= 0.2;
            }
            while(rueckgabebetrag >= 0.1) // 10 CENT-Münzen
            {
                System.out.println("10 CENT");
                rueckgabebetrag -= 0.1;
            }
            while(rueckgabebetrag >= 0.05)// 5 CENT-Münzen
            {
                System.out.println("5 CENT");
                rueckgabebetrag -= 0.05;
            }
        }
    }

}

/* 1. Folgende Variablen werden deklariert :
    zuZahlenderBetrag
    eingezahlterGesamtbetrag
    eingeworfeneMünze
    rückgabebetrag
    Der Datentyp ist bei jeder dieser Variablen double.
    tastatur ist vom Typ Scanner.
    In for-Schleife wird Variable i vom Typ int deklariert.

    2. Folgende Operatoren werden auf die Variablen verwendet:
    einfache Zuweisung: =
    Produkt: *
    Kleiner: <
    Differenz: -
    Additionszuweisung: +=
    Größer: >
    Größer gleich: >=
    Subtraktionszuweisung: -=

    5. Es wurde short verwendet, da Anzahl der Tickets nur ganze Anzahlen sind. Die Möglichkeit eine Anzahl größer als short zu kaufen ist nicht nötig und vielleicht auch nicht sinnvoll bei einem einfachen Fahrkartenautomaten, welcher nur 2 Euro Stücke annimmt.

    6. Anzahl ist short, also eine ganze Zahl. Der Preis wird als double, also Fließkommazahl, eingegeben. Bei der Multiplikation dieser beiden Werte, welche von unterschiedlichem Typ sind, wird automatisch eine implizite Typumwandlung durchgeführt. Das Ergebnis wird daher auch double sein, da dies der größere Typ ist.


    Verwendung Arrays für Auswahl des Farhscheins:
    Vorteilhaft ist, dass die Preise und Bezeichnungen nicht mehr in der Methode hartgecoded sind. Dies führt zu besser lesbarkeit des Codes - z.B. wegen der fehlenden if-Anweisungen - und einfacher Änderung der Preisliste - z.B. wegen Initialisierung der Arrays als Klassenattribut.
    Nachteilig ist, dass es keine feste Kopplung der Preise und Bezeichungen gibt. Ein kleiner Fehler kann schon dazu führen, dass alle Preise nicht mehr stimmen.
 */