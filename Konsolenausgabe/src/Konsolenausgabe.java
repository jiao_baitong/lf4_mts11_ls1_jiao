
public class Konsolenausgabe {

	public static void main(String[] args) {
		//hier werden zwei Tiere beschrieben
		System.out.print("Der Haase ist schnell. ");
		System.out.print("Die Schildkr�te ist langsam. ");
		System.out.print("Die Schildkr�te ist �ber " + 100 +" Jahre alt.\n");
		System.out.print("Die Schildkr�te hei�t \"Schildie\".\n");
		
		System.out.print("      *\n     ***\n    *****\n   *******\n  *********\n ***********\n*************\n     ***\n     ***\n");
		
		System.out.printf("%.2f\n", 22.4234234);
		System.out.printf("%.2f\n", 111.2222);
		System.out.printf("%.2f\n", 4.0);
		System.out.printf("%.2f\n", 1000000.551);
		System.out.printf("%.2f\n", 97.34);
		
	}

}
