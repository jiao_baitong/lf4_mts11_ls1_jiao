import java.util.Scanner;

public class Aufgabe2 {

    public static void main(String[] args) {
        Scanner myScanner = new Scanner(System.in);

        System.out.println("Hallo! Wie ist Ihr Name: ");

        String name = myScanner.next();

        System.out.println("Wie alt sind Sie: ");

        int alter = myScanner.nextInt();

        System.out.println("Ihr Name ist " + name + " und Sie sind " + alter + " Jahre alt.");
    }

}
