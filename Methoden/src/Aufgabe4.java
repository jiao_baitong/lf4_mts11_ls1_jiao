import java.util.Scanner;

public class Aufgabe4 {
    public static final Scanner input = new Scanner(System.in);
    public static void main(String[] args) {
        System.out.println("Das Program berechnet den Ersatzwiderstand zweier parallel geschalteter Widerstände.");
        System.out.print("Geben Sie den Widerstand der ersten Schaltung ein: ");
        double r1 = input.nextDouble();
        System.out.print("Geben Sie den Widerstand der zweiten Schaltung ein: ");
        double r2 = input.nextDouble();
        System.out.print("Der Ersatzwiderstand der Parallelschaltung beträgt: ");
        System.out.println(reihenschaltung(r1, r2));
    }

    public static double reihenschaltung(double r1, double r2) {
        return r1 * r2 / (r1 + r2);
    }
}
