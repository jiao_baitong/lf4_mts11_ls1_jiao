import java.util.Scanner;

public class Mathe {
    public static final Scanner input = new Scanner(System.in);
    public static void main(String[] args) {
        programmAuswahl();
    }

    public static void programmAuswahl(){
        System.out.println("Welches Programm möchten Sie ausführen? (geben Sie die Zahl des Programms an)");
        System.out.println("1. Quadrat");
        System.out.println("2. Hypothenuse");
        int eingabe = input.nextInt();
        if (eingabe == 1) {
            programmQuadrat();
        } else if (eingabe == 2) {
            programmHypothenuse();
        }
    }

    public static void programmQuadrat() {
        System.out.println("Das Program berechnet Quadrat einer Zahl.");
        System.out.print("Geben Sie die Zahl ein: ");
        System.out.printf("Das Quadrat beträgt: " + quadrat(input.nextDouble()));
    }

    public static void programmHypothenuse() {
        System.out.println("Das Program berechnet die Länge der Hypothenuse eines rechtwinkligen Dreiecks aus den Längen der beiden Katheten.");
        System.out.print("Geben Sie die Länge der ersten Kathete ein: ");
        double kathete1 = input.nextDouble();
        System.out.print("Geben Sie die Länge der zweiten Kathete ein: ");
        double kathete2 = input.nextDouble();
        System.out.printf("Das Länge der Hypothenuse beträgt: " + hypothenuse(kathete1, kathete2));
    }

    public static double quadrat(double x) {
        return x * x;
    }

    public static double hypothenuse(double kathete1, double kathete2) {
        return Math.sqrt(quadrat(kathete1) + quadrat(kathete2));
    }
}
