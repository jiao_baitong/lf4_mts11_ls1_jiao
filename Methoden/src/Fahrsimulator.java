import java.util.Scanner;

public class Fahrsimulator {
    public static double v = 0.0;
    public static final Scanner input = new Scanner(System.in);
    public static void main(String[] args) {
        System.out.println("Dies ist ein Fahrsimulator. Das Geschwindigkeitslimit ist 130km/h.");
        fahren();
    }

    public static double beschleunige(double v, double dv) {
        double neueGeschwindigkeit = v + dv;
        if (neueGeschwindigkeit < 0)
            return 0;
        else if (neueGeschwindigkeit > 130)
            return 130;
        return v + dv;
    }

    public static void fahren(){
        do {
            if (v == 0)
                System.out.println("Sie stehen.");
            else System.out.println("Die Aktuelle Geschwindigkeit ist " + v + "km/h");
            System.out.printf("Geben Sie ein um wie viel km/h die Geschwindigkeit geändert werden soll: ");
            v = beschleunige(v, input.nextDouble());
            if (v == 0)
                System.out.println("Sie stehen.");
            else System.out.println("Sie fahren nun mit " + v + "km/h");
            System.out.println("Möchten Sie weiterfahren? (j/n)");
        } while (input.next().equals("j"));
    }
}
