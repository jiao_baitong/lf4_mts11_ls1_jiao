import java.util.Scanner;

public class ABSchleifen2 {
    private static final int ZAHLEN_SYSTEM = 10;
    private static final int MATRIX_GROESSE = 1 * ZAHLEN_SYSTEM;
    private static final Scanner input = new Scanner(System.in);

    public static void main(String[] args) {
        programmAuswahl();
    }

    public static void programmAuswahl() {
        System.out.println("Welches Programm möchten Sie ausführen? (geben Sie die Zahl des Programms an)");
        System.out.println("1. Matrix");
        int eingabe = input.nextInt();
        if (eingabe == 1) {
            programmMatrix();
        }
    }

    private static void programmMatrix() {
        System.out.println("Das Program gibt eine 10x10 Matrix aus, in der alle Zahlen durch einen Stern gekennzeichnet sind, die eine Zahl von 2-9 enthalten,\n durch diese Zahl ohne Rest teilbar sind oder der Quersumme dieser Zahl entsprechen.");
        int zahl;
        do {
            System.out.print("Geben Sie eine Zahl 2-9 ein: ");
            zahl = input.nextInt();
        }
        while (zahl > 9 || zahl < 2);
        int hoehe = 0;
        while (hoehe < MATRIX_GROESSE) {
            int breite = 0;
            while (breite < MATRIX_GROESSE) {
                int auszugebeneZahl = breite + hoehe * MATRIX_GROESSE;
                if (auszugebeneZahl % zahl == 0)
                    System.out.printf("%3s", "*");
                else if (letzteZiffer(auszugebeneZahl) == zahl)
                    System.out.printf("%3s", "*");
                else if (ersteZiffer(auszugebeneZahl) == zahl)
                    System.out.printf("%3s", "*");
                else if (ersteZiffer(auszugebeneZahl) + letzteZiffer(auszugebeneZahl) == zahl)
                    System.out.printf("%3s", "*");
                else System.out.printf("%3d", auszugebeneZahl);
                breite++;
            }
            System.out.println();
            hoehe++;
        }
    }

    private static int ersteZiffer(int i) {
        return (i - i % MATRIX_GROESSE) / MATRIX_GROESSE;
    }

    private static int letzteZiffer(int i) {
        return i % MATRIX_GROESSE;
    }
}
