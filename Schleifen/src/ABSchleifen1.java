import java.util.Scanner;

public class ABSchleifen1 {

    public static final Scanner input = new Scanner(System.in);
    public static void main(String[] args) {
        programmAuswahl();
    }

    public static void programmAuswahl() {
        System.out.println("Welches Programm möchten Sie ausführen? (geben Sie die Zahl des Programms an)");
        System.out.println("1. Quadrat");
        System.out.println("2. Treppe");
        int eingabe = input.nextInt();
        if (eingabe == 1)
            programmQuadrat();
        if (eingabe == 2)
            programmTreppe();
    }

    public static void programmQuadrat() {
        System.out.println("Das Program gibt ein Quadrat im Textmodus aus.");
        System.out.print("Geben Sie eine ganze Zahl für die Größe des Quadrats ein (>1): ");
        int groesse = input.nextInt();
        while (groesse <= 1) {
            System.out.print("Geben Sie eine ganze Zahl für die Größe des Quadrats ein (>1): ");
            groesse = input.nextInt();
        }
        quadratAusgeben(groesse);
    }
    
    public static void quadratAusgeben(int groesse) {
        for (int i = 0; i < groesse; i++) {
            System.out.print("*  ");
        }
        System.out.println();
        for (int i = 0; i < groesse-2; i++) {
            System.out.print("*  ");
            for (int j = 0; j < groesse-2; j++) {
                System.out.print("   ");
            }
            System.out.println("*");
        }
        for (int i = 0; i < groesse; i++) {
            System.out.print("*  ");
        }
    }

    public static void programmTreppe() {
        System.out.println("Das Programm zeichnet eine Treppe aus h Stufen einer bestimmten Breite b.");
        System.out.println("Geben Sie h ein (ganze positive Zahl):");
        int h = input.nextInt();
        System.out.println("Geben Sie b ein (ganze positive Zahl):");
        int b = input.nextInt();
        int treppenbreite = b * h;
        StringBuilder treppenStufe = new StringBuilder("");
        for (int i = 0; i < b; i++) {
            treppenStufe.append("*");
        }
        StringBuilder treppenEbene = new StringBuilder("");
        for (int i = 0; i < h; i++) {
            treppenEbene.append(treppenStufe);
            System.out.printf("%" + treppenbreite + "s%n", treppenEbene);
        }
    }
}
